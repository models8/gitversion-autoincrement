DOCKER_IMAGE = gittools/gitversion:5.10.1-alpine.3.14-6.0
CURRENT_DIR = $$(pwd)
CONTAINER_DIR = /repo
REPOT_URL = https://github.com/enzo-cora/gitversion-test-autoincrement.git
COMMIT_REF_NAME = master
COMMIT_SHA = 5adc47659cd0d293d0f7a615056132e56678ba20

gitversion:
	docker run --rm -v "$(CURRENT_DIR):$(CONTAINER_DIR)" $(DOCKER_IMAGE) $(CONTAINER_DIR) | grep -i "FullSemVer"
	
gitversion-url:
	docker run --rm $(DOCKER_IMAGE) -url $(REPOT_URL) -b $(COMMIT_REF_NAME) -c $(COMMIT_SHA) 

increment:
	echo "y" >> README.md && git commit -a -m"feat: edit readme" && make gitversion | grep -i FullSemVer
